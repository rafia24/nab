package CucumberPageObjectModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import junit.framework.Assert;

public class dashBoardPage extends baseclass {

@FindBy(css = "#menu-logo-container > nav > ul > li.active > a") WebElement personalTab;
@FindBy(css = "#menu-logo-container > nav > ul > li.active.open > ul > li:nth-child(5) > a") WebElement homeLoan;

@FindBy(xpath = "//*[@id=\"menu-logo-container\"]/nav/ul/li[1]/ul/li[5]/div/ul/li[1]/a") WebElement homeLoanLink;

@FindBy(xpath = "//*[@id=\"experience\"]/div/div/div/section/div/div/div/div[3]/div/div[3]/div/div/div[2]/a") WebElement enquireNow;
@FindBy(xpath = "//*[@id=\"wrapper\"]/div/div/section/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[2]/div/p[2]/a") WebElement callBack;
@FindBy(css = "input[value='Search']") WebElement searchbutton;

@FindBy(xpath = "//*[@id=\"experience\"]/div/div/div/section/div/div/div/div[3]/div/div[2]/div[2]") WebElement scrollEnquire;
@FindBy(css = "#myRadioButton-0 > label") WebElement newLoan;
@FindBy(css = "#field-page-Page1-isExisting > label:nth-child(2)") WebElement noCustomer;
@FindBy(css = "#field-page-Page1-aboutYou-firstName") WebElement firstName;
@FindBy(css = "#field-page-Page1-aboutYou-lastName") WebElement lastName;

@FindBy(css = "#field-page-Page1-aboutYou-phoneNumber") WebElement phoneNumber;
@FindBy(css = "#field-page-Page1-aboutYou-email") WebElement emailForm;

@FindBy(css = "#page-Page1-btnGroup-submitBtn") WebElement submitbtn;





	public dashBoardPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void setDashboardFields() throws InterruptedException {
		Thread.sleep(2000);
		clickevent(personalTab);
		Thread.sleep(2000);
		clickevent(homeLoan);
		Thread.sleep(2000);
		clickevent(homeLoanLink);
	}
	public void moreNewFilters() throws InterruptedException {
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scrollEnquire);
		
		clickevent(enquireNow);
		clickevent(callBack);
	}
	
	public void fillingForm() throws InterruptedException {
		  WebElement element = shadow.findElement("#myRadioButton-0 > label");
		  

		clickevent(element);
	
	}
	
	public void nextbutton() throws InterruptedException {
	  
		  WebElement nextbtn = shadow.findElement("#main-container button[data-component-id='Button']"); 
		
		  Thread.sleep(2000);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", nextbtn);
		  clickevent(nextbtn);
	}
	
	public void submitform(String fname, String lname, String pno, String email) throws InterruptedException {
		  Thread.sleep(3000);
		  String windowHandle = driver.getWindowHandle();

		//Get the list of window handles
		ArrayList tabs = new ArrayList (driver.getWindowHandles());
		System.out.println(tabs.size());
		//Use the list of window handles to switch between windows
		driver.switchTo() .window((String) tabs.get(1));
		clickevent(noCustomer);
		SendKeysevent(firstName, fname);
		SendKeysevent(lastName, lname);
		SendKeysevent(phoneNumber, pno);
		SendKeysevent(emailForm, email);
		//clickevent(submitbtn);
		
	}
}
